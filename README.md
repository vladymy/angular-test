# Angular test

## setup 

To run this app you will need nodejs, npm, grunt, and bower. So please install it

Run `npm install` then `bower install` to install all necessary packages 

## Build & development

Run `grunt` for building and `grunt serve` for preview.

