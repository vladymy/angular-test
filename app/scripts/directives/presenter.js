'use strict';

/**
 * @ngdoc directive
 * @name testsApp.directive:Presenter
 * @description
 * # Presenter
 */
angular.module('testsApp')
  .directive('presenter', [
    '$timeout',
    function (timeout) {
      return {
        restrict: 'E',
        link: function postLink(scope, element, attrs) {

          scope.runMsgCleaner = function() { //clean messages when life period is finished
            timeout(function() { // wait until all views is rendered              
              element.find('.msg-box').each(function(id, item) { // go through all added messages
                var msgLifetime = $(item).attr('lifetime'); // get curent message life period
                if (msgLifetime) {// if life period not a 0 delete it. If it's 0 save this message.
                  $(item)
                    .delay(msgLifetime*1000) 
                    .fadeOut(300, function() {
                      scope.messages.splice(id); // when this msg disappear remove it from the scope
                    });
                }
              })
            });
          }
        }
      };
  }]);
