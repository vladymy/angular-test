'use strict';

/**
 * @ngdoc overview
 * @name testsApp
 * @description
 * # testsApp
 *
 * Main module of the application.
 */
angular
  .module('testsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })      
      .otherwise({
        redirectTo: '/'
      });
  });
