'use strict';

/**
 * @ngdoc function
 * @name testsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testsApp
 */
angular.module('testsApp')
  .controller('MainCtrl', [
    '$scope',
    'Consumer',
    function ($scope, Consumer) {

      $scope.loadMsg = function() { // handler on btn click
        var promise = Consumer.getMessages(); // load messages via Consumer service

        promise.then(function(messages) {
          $scope.messages = messages; // put messages in to the scope
        });        

        $scope.$watch('messages', function(newValue, oldValue, scope) {          
          if(newValue){
            scope.runMsgCleaner();// when $scope.messages are loaded run message cleaner. See Presenter directive
          }          
        });
      };
  }]);
