'use strict';

/**
 * @ngdoc service
 * @name testsApp.Consumer
 * @description
 * # Consumer
 * Factory in the testsApp.
 */
angular.module('testsApp')
  .factory('Consumer', [
    '$location',
    '$http',
    '$q',
    function (location, http, q) {
      // Service logic
      var APIUrl = location.protocol() + '://' + location.host() + ":" + location.port() + '/json/msg-data.json';

      var _getMessages = function() {
        var deferred = q.defer(); // create a defer

        //send ajax request to get data with messages
        http.get(APIUrl).
          success(function(data, status, headers, config) {
            if (data && data.length > 0) { // if there is a data resolve it
              deferred.resolve(data);
            }
          }).
          error(function(data, status, headers, config) {
            console.log(data); // if there is some error show it
          });

        return deferred.promise; // return promises
      };

      // Public API here
      return {
        getMessages: _getMessages
      };
  }]);
