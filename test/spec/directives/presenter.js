'use strict';

describe('Directive: Presenter', function () {

  // load the directive's module
  beforeEach(module('testsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-presenter></-presenter>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the Presenter directive');
  }));
});
