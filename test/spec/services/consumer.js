'use strict';

describe('Service: Consumer', function () {

  // load the service's module
  beforeEach(module('testsApp'));

  // instantiate service
  var Consumer;
  beforeEach(inject(function (_Consumer_) {
    Consumer = _Consumer_;
  }));

  it('should do something', function () {
    expect(!!Consumer).toBe(true);
  });

});
